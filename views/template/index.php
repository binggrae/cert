<?php

use app\models\Template;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список шаблонов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="font-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить шаблон', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function (Template $model) {
                    return Html::a($model->title, ['/admin/view', 'id' => $model->id]);
                }

            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function (Template $template) {
                    return Html::a($template->getStatusLabel(true), ['/template/status', 'id' => $template->id]);
                }
            ],
            [
                'class' => ActionColumn::className(),
                'template' => '{view} {update} {delete}'
            ]
        ],
    ]); ?>
</div>
