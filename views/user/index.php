<?php

use app\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'username',
                'format' => 'raw',
                'value' => function (User $user) {
                    return Html::a($user->username, ['/user/view', 'id' => $user->id]);
                }
            ],
            [
                'attribute' => 'email',
                'format' => 'raw',
                'value' => function (User $user) {
                    return Html::a($user->email, ['/user/view', 'id' => $user->id]);
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function (User $user) {
                    return Yii::$app->formatter->asDatetime($user->created_at);
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function (User $user) {
                    return $user->getStatusLabel(true);
                }
            ],
            [
                'attribute' => 'role',
                'format' => 'raw',
                'value' => function (User $user) {
                    return $user->getRoleLabel(true);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'
            ],
        ],
    ]); ?>
</div>
