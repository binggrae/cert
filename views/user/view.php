<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
            'email:email',
            [
                'attribute' => 'created_at',
                'value' => Yii::$app->formatter->asDatetime($model->created_at)
            ], [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => $model->getStatusLabel(true)
            ], [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => $model->getRoleLabel(true)
            ]
        ],
    ]) ?>

</div>
