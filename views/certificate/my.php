<?php

use app\models\Certificate;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мои сертификаты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="font-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items}',
        'columns' => [
            'id',
            [
                'attribute' => 'title',
                'value' => function (Certificate $model) {
                    return $model->template->title;
                }
            ],
            [
                'header' => '',
                'format' => 'raw',
                'value' => function (Certificate $model) {
                    return Html::a('Просмотреть', ['/certificate/view', 'id' => $model->id], [
                        'class' => 'btn btn-success'
                    ]);
                }
            ],
            [
                'header' => '',
                'format' => 'raw',
                'value' => function (Certificate $model) {
                    return Html::a('Скачать', ['/certificate/download', 'id' => $model->id], [
                        'class' => 'btn btn-primary',
                        'target' => '_blank'
                    ]);
                }
            ]

        ],
    ]); ?>
</div>
