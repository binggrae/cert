<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => [
            [
                'label' => 'Пользователи',
                'url' => ['/user/index'],
                'visible' => Yii::$app->user->can('admin'),
            ],
            [
                'label' => 'Шаблоны',
                'url' => ['/template/index'],
                'visible' => Yii::$app->user->can('admin'),
            ],
            [
                'label' => 'Сертификаты',
                'url' => ['/template/certificates'],
                'visible' => Yii::$app->user->can('admin'),
            ],
            [
                'label' => 'Шрифты',
                'url' => ['/font/index'],
                'visible' => Yii::$app->user->can('admin'),
            ],
            [
                'label' => 'Создать сертификат',
                'url' => ['/certificate/index'],
            ],
            [
                'label' => 'Мои сертификаты',
                'url' => ['/certificate/my'],
                'visible' => !Yii::$app->user->isGuest
            ],
        ],
    ]);

    if (Yii::$app->user->isGuest) {
        $rightItems[] = ['label' => 'Войти', 'url' => ['/site/login']];
        $rightItems[] = ['label' => 'Регистрация', 'url' => ['/site/signup']];
    } else {
        $rightItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Выход (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $rightItems,
    ]);

    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
