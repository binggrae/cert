<?php

use yii\db\Migration;

/**
 * Class m180121_085312_fix_value
 */
class m180121_085312_fix_value extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->dropForeignKey('fk_certValue_cert','cert_values');
        $this->dropForeignKey('fk_certValue_block','cert_values');
        $this->dropTable('cert_values');

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }


        $this->createTable('cert_values', [
            'cert_id' => $this->integer()->notNull(),
            'block_id' => $this->integer()->notNull(),
            'value' => $this->string(),
        ], $tableOptions);
        $this->addForeignKey('fk_certValue_cert',
            'cert_values', 'cert_id',
            'certificates', 'id',
            'CASCADE', 'RESTRICT');

        $this->addForeignKey('fk_certValue_block',
            'cert_values', 'block_id',
            'blocks', 'id',
            'CASCADE', 'RESTRICT');

        $this->addPrimaryKey('pk_certValues', 'cert_values', ['cert_id', 'block_id']);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180121_085312_fix_value cannot be reverted.\n";

        return false;
    }
    */
}
