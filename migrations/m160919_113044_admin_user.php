<?php

use yii\db\Migration;

class m160919_113044_admin_user extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'role', $this->integer()->notNull()->defaultValue(0));

    }

    public function down()
    {
        $this->dropColumn('user', 'role');
    }

}
