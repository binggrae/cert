<?php

use yii\db\Migration;

/**
 * Class m180121_175618_field
 */
class m180121_175618_field extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('types', [
            'type' => 'id',
            'title' => 'ID'
        ]);

        $this->insert('types', [
            'type' => 'day',
            'title' => 'День'
        ]);

        $this->insert('types', [
            'type' => 'month',
            'title' => 'Месяц'
        ]);

        $this->insert('types', [
            'type' => 'year',
            'title' => 'Год'
        ]);

        $this->insert('types', [
            'type' => 'date',
            'title' => 'Дата'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180121_175618_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180121_175618_field cannot be reverted.\n";

        return false;
    }
    */
}
