<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 19.09.2016
 * Time: 16:58
 */

namespace app\commands;


use app\rbac\UserRoleRule;
use yii\console\Controller;

class RbacController extends Controller
{

    public function actionInit()
    {
        $authManager = \Yii::$app->authManager;
        $authManager->removeAll();


        // Create roles
        $guest = $authManager->createRole('guest');
        $user = $authManager->createRole('user');
        $admin = $authManager->createRole('admin');


        // Create simple, based on action{$NAME} permissions
        $login = $authManager->createPermission('login');
        $logout = $authManager->createPermission('logout');
        $error = $authManager->createPermission('error');
        $signUp = $authManager->createPermission('sign-up');

        $client = $authManager->createPermission('client');
        $manager = $authManager->createPermission('manager');


        // Add permissions in Yii::$app->authManager
        $authManager->add($login);
        $authManager->add($logout);
        $authManager->add($error);
        $authManager->add($signUp);
        $authManager->add($client);
        $authManager->add($manager);


        // Add rule, based on UserExt->group === $user->group
        $userRoleRule = new UserRoleRule();
        $authManager->add($userRoleRule);

        // Add rule "UserGroupRule" in roles
        $guest->ruleName = $userRoleRule->name;
        $user->ruleName = $userRoleRule->name;
        $admin->ruleName = $userRoleRule->name;

        // Add roles in Yii::$app->authManager
        $authManager->add($guest);
        $authManager->add($user);
        $authManager->add($admin);

        // Add permission-per-role in Yii::$app->authManager
        // Guest
        $authManager->addChild($guest, $login);
        $authManager->addChild($guest, $logout);
        $authManager->addChild($guest, $error);
        $authManager->addChild($guest, $signUp);

        // User
        $authManager->addChild($user, $client);
        $authManager->addChild($user, $guest);

        // Admin
        $authManager->addChild($admin, $manager);
        $authManager->addChild($admin, $user);
    }

}