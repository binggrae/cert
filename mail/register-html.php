<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\User */

?>
<div class="register">
    <p>Здравствуй <?= Html::encode($user->username) ?>,</p>

    <p>Пароль для входа на сайт: <?= $user->password; ?></p>
</div>

