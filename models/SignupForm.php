<?php

namespace app\models;

use yii\base\Model;
use app\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     * @throws \yii\base\Exception
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        if (YII_ENV == YII_ENV_DEV) {
            $user->role = User::ROLE_ADMIN;
        }

        if ($user->save()) {
        //if (1) {
			if(\Yii::$app->user->isGuest) {
				\Yii::$app->user->login($user, 3600 * 24 * 30);
			} elseif(\Yii::$app->user->identity->role != User::ROLE_ADMIN) {
				\Yii::$app->user->login($user, 3600 * 24 * 30);
			}

            $a = \Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'register-html', 'text' => 'register-text'],
                    ['user' => $this]
                )
                ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name . ' robot'])
                ->setTo($this->email)
                ->setSubject('Регистрация на сайте ' . \Yii::$app->name)
                ->send();
			

            return $user;
        }

        return null;
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Имя пользователя',
            'password' => 'Пароль'

        ];
    }

    public static function getRandomPassword()
    {
        $chars = "qazxswedcvfrtgbnhyujmkiolp1234567890";

        $max = 6;
        $size = StrLen($chars) - 1;

        $password = null;

        while ($max--) {
            $password .= $chars[rand(0, $size)];
        }

        return $password;
    }
}
